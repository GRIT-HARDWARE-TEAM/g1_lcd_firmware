/*
 * lcd_driver_settings.h
 *
 *  Created on: 24 Apr 2019
 *  Author: Mubarak Okunade
 */

#ifndef LCD_DRIVER_INC_LCD_DRIVER_SETTINGS_H_
#define LCD_DRIVER_INC_LCD_DRIVER_SETTINGS_H_

/**
 * @brief LCD controller selectors. Selected LCD controller should be uncommented.
 */
	//#define ILI9341					1
	//#define ILI9225 					1
	//#define DOTMAT_16X2 				1
	#define HD44780_DOTMAT_20X4 		1



/**
 * @brief LCD settings.
 */
	static const bool BACKLIGHT_DIMMER_ON = false;
	static const uint16_t BACKLIGHT_TIMEOUT = 10000;		// backlight duration setting in ms.
	static const uint8_t DIM_BRIGHTNESS_SETTING = 25;		// backlight dimmer setting.
	static const uint8_t MAX_BRIGHTNESS_SETTING = 100;		// backlight max brightness setting.

#endif /* LCD_DRIVER_INC_LCD_DRIVER_SETTINGS_H_ */
