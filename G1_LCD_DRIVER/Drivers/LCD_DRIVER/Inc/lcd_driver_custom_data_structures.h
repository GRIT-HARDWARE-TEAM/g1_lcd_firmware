/*
 * lcd_driver_custom_data_structures.h
 *
 *  Created on: 24 Apr 2019
 *  Author: Mubarak Okunade
 */

#ifndef LCD_DRIVER_INC_LCD_DRIVER_CUSTOM_DATA_STRUCTURES_H_
#define LCD_DRIVER_INC_LCD_DRIVER_CUSTOM_DATA_STRUCTURES_H_

/**
 * @brief bool type definition.
 */
typedef int bool;
enum {false,true};

/**
 * @brief LCD DRIVER tenant settings
 * dictates the maximum number of tenants this driver can support
 */

#define MAX_NO_OF_TENANTS	4


// parameter data structure
typedef struct{
	char name[24];
	uint8_t valueType;	// 1 = number	// 0 = string
	uint8_t fontSelector;	//	1 = calculatrix font	// 0 = 5x5 font
	float nValue;
	char sValue[24];
	uint8_t standAloneStatus;
	bool updateFlag;
}parameter;

// icon states data structure
/**
 *@brief icon state data structure
 */
typedef int iconState;
enum {
	OK,
	NOT_OK,
	OVER_VOLTAGE_FAULT,
	OVER_CURRENT_FAULT,
	CASE_TAMPER_EVENT ,
	MAGNETIC_TAMPER_EVENT
};

// icon data structure
/**
 *@brief icon data structure.
 */
typedef struct{
	char symbolChar[4];
	uint16_t x,y;
	iconState state;
	parameter stateTxt;
}icon;

/**
 * @brief service message data structure.
 * property message - text to be printed on the display.
 * @property x 	- text position on the x - axis.
 * @property y	- text position on the y - axis.
 */
typedef struct{
	char message[32];
	uint16_t x,y;
	uint16_t delay;
}serviceMessage;

#endif /* LCD_DRIVER_INC_LCD_DRIVER_CUSTOM_DATA_STRUCTURES_H_ */
