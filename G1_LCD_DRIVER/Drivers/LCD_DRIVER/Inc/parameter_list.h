/**
 * @file parameter_list.h
 * @brief It stores parameter names and their respective dummy values.
 * 		  All items in the array was used in early tests. But now only the parameter names here are being used.
 *
 * @date 2 Nov 2018
 * @author Mubarak Okunade
 */

#ifndef LCD_DRIVER_INC_PARAMETER_LIST_H_
#define LCD_DRIVER_INC_PARAMETER_LIST_H_

/**
 * @brief menu settings.
 */
#define NO_OF_PARAMETERS		23

static char PARAMETER_LIST[NO_OF_PARAMETERS][24] = {
														  "LCD_TEST",
														  "DATE",
														  "REAL TIME",
														  "ACTIVE ENERGY",
														  "INST. KW",
														  "REACTIVE ENERGY",
														  "INST. KVAr",
														  "APPARENT ENERGY",
														  "INST. KVA",
														  "MAXIMUM DEMAND(KW)",
														  "MAXIMUM DEMAND(KVA)",
														  "MD RESET COUNT",
														  "INST. PF",
														  "INST. FREQ(HZ)",
														  "INST. VOLTAGE(V)",
														  "INST. CURRENT(A)",
														  "BILLING(KWh)",
														  "TOD energy(KWh)",
														  "TOD Max demand(KVA)",
														  "CUMUL. MAX DEMAND",
														  "TAMPER SWITCH STAT.",
														  "LAST OCCRNC. OF ABN.",
														  "LTST RESTN. OF ABN."
												   };



#endif /* LCD_DRIVER_INC_PARAMETER_LIST_H_ */
