/**
 * @file		lcd_driver.h
 * @brief 		The LCD driver serves as an interface between the firmware programmer and
			   	different types of LCDs by providing an interface with
			   	functions common to all of them.
 * 				ili9341_lcd_driver.c relies on low level functions written by Matej Artnak.
			  	Thanks Matej!.

 * @date 		31 Oct 2018.
 * @author 		Mubarak Okunade.
 *
 */

#ifndef LCD_DRIVER_INC_LCD_DRIVER_H_
#define LCD_DRIVER_INC_LCD_DRIVER_H_

//#include <parameter_list.h>
#include "stdio.h"
#include "stm32f7xx_hal.h"
#include "tim.h"
#include "gpio.h"
#include "color_codes.h"
#include "lcd_driver_custom_data_structures.h"
#include "lcd_driver_settings.h"
#include "lcd_driver_ui_layout_settings.h"



// function prototypes

// parameter initializer
void initializeParameters();

/**
 * @brief converts floating point numbers to strings that will be displayed on the screen.
 * @param number decimal number to converted to character strings.
 * @param string holds resulting string after conversion.
 * @returns void.
 */
void floatToString(float number, char string[]);

/**
 * @brief sets up the LCD, clears the screen and sets the display mode to landscape depending on which type of LCD.
 * @returns void.
 */
void setup();

/**
 * @brief clears the LCD by repainting it in its original background colour.	LOL!!!
 * @returns void.
 */
void clear();

/**
 * @brief prints text on the LCD at a position defined by x,y and other attributes as defined as parameters of the function.
 * @param text 					- 			text to be printed on the display.
 * @param x    					- 			text position on the x - axis.
 * @param y						- 			text position on the y - axis.
 * @param colour				- 			text colour.
 * @param size					- 			text size.
 * @param background_colour		- 			background colour on the display.
 * @returns void
 */
void print(const char* text, uint16_t x, uint16_t y, uint16_t colour, uint16_t size, uint16_t background_colour);

/**
 * @brief prints text on the LCD at a position defined by x,y and other attributes as defined as parameters of the function.
 * @param text					- 			text to be printed on the display.
 * @param x						- 			text position on the x - axis.
 * @param y						- 			text position on the y - axis.
 * @param colour				- 			text colour.
 * @param size					- 			text size.
 * @param background_colour		- 			background colour on the display.
 * @returns void.
 */
void printn(const char* text, uint16_t x, uint16_t y, uint16_t colour, uint16_t size, uint16_t background_colour);

/**
 * @brief prints a service message on the LCD at a position defined by x,y with the default colour and font size settings.
 * @param text					- 			text to be print on the display.
 * @param x						- 			text position on the x - axis.
 * @param y						- 			text position on the y - axis.
 * @returns void.
 */
void printServiceMessage(const char* text, uint16_t x , uint16_t y);

/**
 * @brief prints a service message on the LCD at a position defined by x,y with the default colour and font size settings.
 * @param service_messages 		- 			array of service_messages.
 * @param n				   		- 			number of elements in the array.
 * @param serviceMessageDelay   - 		pointer to the No of ms the message box stays on the screen.
 * @returns void.
 */
void printServiceMessages(serviceMessage service_messages[],uint8_t n,uint16_t * serviceMessageDelay);

/**
 * @brief prints the parameter value under the parameter name.
 * @param parameter_number		- 			parameter number identifying the menu page item you want to print on the display.
 * @param section_number		- 			section number identifying the part of the page you want to print the menu item on.
 * @returns void.
 */
//void printParameterValue(intmax_t parameter_number,uint8_t section_number);
void printParameterValue(parameter param,uint8_t section_number);


/**
 * @brief prints the parameter name.
 * @param parameter_number 		- 			parameter number identifying the menu page title you want to print on the display.
 * @param section_number		- 			section number identifying the part of the page you want to print the menu title on.
 * @returns void.
 */
//void printParameterName(intmax_t parameter_number,uint8_t section_number);
void printParameterName(parameter param,uint8_t section_number);

void printDefaultPage(bool navFlag);

/**
 * @brief copies the date from the local "lcd_test_txt" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the lcd test txt.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getLCDTestTxt(char data[]);


/**
 * @brief copies the date from the local "date" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the date.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getDate(char data[]);

/**
 * @brief copies the real time from the local "real_time" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the real time.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getRealTime(char data[]);

/**
 * @brief copies the active energy from the local "active_energy" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the active_energy.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getActiveEnergy(char data[]);

/**
 * @brief copies the reactive energy from the local "reactive_energy" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the reactive_energy.
 *											This is what will be displayed on the screen.
 *
 * @returns void.
 */
void getReactiveEnergy(char data[]);

/**
 * @brief copies the apparent energy from the local "apparent_energy" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the apparent_energy.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getApparentEnergy(char data[]);

/**
 * @brief copies the maximum demand in KW from the local "max_demand_kw" variable into the "data" variable.
 * @param data					-			data array holding a copy of the max_demand_kw.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getMaxDemandKW(char data[]);

/**
 * @brief copies the maximum demand in KVA from the local "max_demand_kva" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the max_demand_kva.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getMaxDemandKVA(char data[]);

/**
 * @brief copies the md reset count from the local "md_reset_count" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the md_reset_count.
 *										    This is what will be displayed on the screen.
 * @returns void.
 */
void getMDResetCount(char data[]);

/**
 * @brief copies the instant power factor from the local "inst_pf" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the inst_pf.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getInstPF(char data[]);

/**
 * @brief copies the instant frequency from the local "inst_freq" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the inst_freq.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getInstFreq(char data[]);

/**
 * @brief copies the instant voltage from the local "inst_voltage" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the inst_voltage.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getInstVoltage(char data[]);

/**
 * @brief copies the instant current from the local "inst_current" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the inst_current.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getInstCurrent(char data[]);

/**
 * @brief copies the instant real power from the local "inst_kw" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the inst_kw.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getInstKW(char data[]);


/**
 * @brief copies the instant reactive power from the local "inst_kvar" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the inst_kvar.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getInstKVAr(char data[]);

/**
 * @brief copies the instant apparent power from the local "inst_kva" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the inst_kva.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getInstKVA(char data[]);

/**
 * @brief copies the billing data from the local "billing" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the billing.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getBilling(char data[]);

/**
 * @brief copies the tod energy from the "tod_energy_kwh" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the tod_energy_kwh.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getTodEnergyKWh(char data[]);

/**
 * @brief copies the tod max apparent power demand from the "tod_max_demand_kva" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the tod_max_demand_kva.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getTodMaxDemandKVA(char data[]);

/**
 * @brief copies the cumulative max demand from the "cumul_max_demand" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the cumul_max_demand.
 *										    This is what will be displayed on the screen.
 * @returns void.
 */
void getCumulMaxDemand(char data[]);

/**
 * Deprecated. Use getStateTextFromState().
 * @brief copies the tamper switch status from the "tamper_switch_stat" variable into the "data" variable.
 * @param tamper_switch_stat	- 			tamper_switch_stat parameter holding tamper switch status.
 *		 									A variable with the same name will be passed as a parameter
 *		 									and updated as needed by the programmer.
 * @param data					- 			data array holding a copy of the tamper_switch_stat parameter.
 *										    This is what will be displayed on the screen.
 */
void getTamperSwitchStat(char data[]);

/**
 * @brief copies the last occurrence of abnormality from the "last_of_occur_of_abn" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the last_of_occur_of_abn.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getLastOccurOfAbn(char data[]);

/**
 * @brief copies the latest restoration of abnormality from the "ltst_restn_of_abn" variable into the "data" variable.
 * @param data					- 			data array holding a copy of the ltst_restn_of_abn.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void getLtstRestnOfAbn(char data[]);

/**
 * @brief copies the data of a corresponding parameter number into the data variable.
 * @param parameter_number		- 			parameter_number parameter holding the page number of the page needed.
 * @param data					- 			data array holding a copy of the data on the corresponding page.
 *											This is what will be displayed on the screen.
 * @returns void.
 */
void get(intmax_t parameter_number, char data[], parameter * param);

/**
 * @brief prints the menu page using the printParameterName and printParameterValue.
 * @param parameter_number
 * @returns void.
 */
void printMenuPage(intmax_t * parameter_number);

/**
 * @brief 	controls the backlight with a boolean variable "on".
 *  	  	"on" can be set to either true or false.
 *        	true makes the backlight go on.
 *        	false makes it go off.
 * @param 	on					-			boolean variable controlling the state of the LCD backlight.
 * @returns void.
 */
void backlight(bool on);

/**
 * @brief 			controls backlight brightness with a boolean variable "status".
 *  				"status" can be set to either true or false.
 *  				true makes the backlight go dim.
 *  				false makes it go bright.
 * @param status				- 			boolean variable controlling the brightness of the LCD backlight.
 */
void dimBacklight(bool status);

/**
 * @brief				controls the backlight brightness level(ranging from 0 - 100) with PWM.
 * @param brightness			- 			the brightness parameter is an unsigned integer with a range of 0 - 100.
 */
void setBrightnessPWM(uint8_t brightness);

/**
 * @brief draws the menu page which can display two parameters at a time.
 * @returns void.
 */
void drawPage();

/**
 * @brief draws a message box that can be used for displaying service messages.
 * @returns void.
 */
void drawMessageBox();

/**
 * @brief wipes off the service message box.
 */
void clearServiceMessageBox();

/**
 * @brief draws an icon at a point x,y on the screen.
 * @param symbolChars			-			the character(s) that make up the symbol.
 * @param x						-			x coordinate on the screen.
 * @param y						-			y coordinate on the screen.
 * @param color					-			color of the icon.
 * @returns void.
 */
void drawIcon(char symbolChars[], uint16_t x,uint16_t y,uint16_t color);

/**
 * @brief draws a section of the screen which may be indicated by a visible line.
 */
void drawIconSection();

/**
 * @brief initializes icons to their default state.
 */
void initializeIcons();

// update icon state

/**
 * @brief updates an icon's state.
 * @param statusIcon	-	icon whose state is to be updated.
 * @param statusIconState - icon state.
 */
void updateIconState(icon statusIcon, iconState statusIconState);

/**
 * @brief updates OV icon's state.
 * @param statusIconState - icon state.
 */
void updateOVIconState(iconState statusIconState);

/**
 * @brief updates OC icon's state.
 * @param statusIconState - icon state.
 */
void updateOCIconState(iconState statusIconState);

/**
 * @brief updates TS icon's state.
 * @param statusIconState - icon state.
 */
void updateTSIconState(iconState statusIconState);

/**
 * @brief updates CS icon's state.
 * @param statusIconState - icon state.
 */
void updateCSIconState(iconState statusIconState);

/**
 *
 * @param state
 */
uint16_t getIconColorFromState(iconState state);

/**
 *
 * @param state - icon state
 * @param stateText	-	icon state in words.
 * @returns void.
 */
void setIconParameterProperty(icon * icon);

/**
 * @brief copies the date from the "e_lcd_test_txt" parameter into the local "lcd_test_txt" variable.
 * @param e_lcd_test_txt					- 			e_lcd_test_txt array holding the actual date.
 * @returns void.
 */
void updateLCDTestTxt(char e_lcd_test_txt[]);

/**
 * @brief copies the date from the "e_date" parameter into the local "date" variable.
 * @param e_date					- 			e_date array holding the actual date.
 * @returns void.
 */
void updateDate(char e_date[]);

/**
 * @brief copies the real time from the "e_real_time" parameter into the local "real_time" variable.
 * @param e_real_time				- 			e_real_time array holding the real time.
 * @returns void.
 */
void updateRealTime(char e_real_time[]);

/**
 * @brief copies the active energy from the "e_active_energy" parameter into the local "active_energy" variable.
 * @param e_active_energy			- 			e_active_energy parameter holding active energy.
 * @returns void.
 */
void updateActiveEnergy(float e_active_energy);

/**
 * @brief copies the reactive energy from the "e_reactive_energy" parameter into the local "reactive_energy" variable.
 * @param e_reactive_energy 		- 			e_reactive_energy parameter holding reactive energy. *
 * @returns void.
 */
void updateReactiveEnergy(float e_reactive_energy);

/**
 * @brief copies the apparent energy from the "e_apparent_energy" parameter into the local "apparent_energy" variable.
 * @param e_apparent_energy		- 			e_apparent_energy parameter holding apparent energy.
 * @returns void.
 */
void updateApparentEnergy(float e_apparent_energy);

/**
 * @brief copies the maximum demand in KW from the "e_max_demand_kw" parameter into the local "max_demand_kw" variable.
 * @param e_max_demand_kw			- 			e_max_demand_kw parameter holding maximum demand in KW.
 * @returns void.
 */
void updateMaxDemandKW(float e_max_demand_kw);

/**
 * @brief copies the maximum demand in KVA from the "e_max_demand_kva" parameter into the local "max_demand_kva" variable.
 * @param e_max_demand_kva		- 			e_max_demand_kva parameter holding maximum demand in KVA.
 * @returns void.
 */
void updateMaxDemandKVA(float e_max_demand_kva);

/**
 * @brief copies the md reset count from the "e_md_reset_count" parameter into the local "md_reset_count" variable.
 * @param e_md_reset_count		- 			e_md_reset_count parameter holding md reset count.
 * @returns void.
 */
void updateMDResetCount(unsigned int e_md_reset_count);

/**
 * @brief copies the instant power factor from the "e_inst_pf" parameter into the local "inst_pf" variable.
 * @param e_inst_pf				- 			e_inst_pf parameter holding the instant power factor.
 * @returns void.
 */
void updateInstPF(float e_inst_pf);

/**
 * @brief copies the instant frequency from the "e_inst_freq" parameter into the local "inst_freq" variable.
 * @param e_inst_freq 			- 			e_inst_freq parameter holding instant frequency.
 * @returns void.
 */
void updateInstFreq(float e_inst_freq);

/**
 * @brief copies the instant voltage from the "e_inst_voltage" parameter into the local "inst_voltage" variable.
 * @param e_inst_voltage			- 			e_inst_voltage parameter holding instant voltage.
 * @returns void.
 */
void updateInstVoltage(float e_inst_voltage);

/**
 * @brief copies the instant current from the "e_inst_current" parameter into the local "inst_current" variable.
 * @param e_inst_current			- 			e_inst_current parameter holding instant current.
 * @returns void.
 */
void updateInstCurrent(float e_inst_current);

/**
 * @brief copies the instant real power from the "e_inst_kw" parameter into the local "inst_kw" variable.
 * @param e_inst_kw				- 			e_inst_kw parameter holding instant real power.
 * @returns void.
 */
void updateInstKW(float e_inst_kw);


/**
 * @brief copies the instant reactive power from the "e_inst_kvar" parameter into the local "inst_kvar" variable.
 * @param e_inst_kvar				- 			e_inst_kvar parameter holding instant reactive power.
 * @returns void.
 */
void updateInstKVAr(float e_inst_kvar);

/**
 * @brief copies the instant apparent power from the "e_inst_kva" parameter into the local "inst_kva" variable.
 * @param e_inst_kva				- 			e_inst_kva parameter holding instant apparent power.
 * @returns void.
 */
void updateInstKVA(float e_inst_kva);

/**
 * @brief copies the billing data from the "e_billing" parameter into the local "billing" variable.
 * @param e_billing				- 			e_billing parameter holding billing data.
 * @returns void.
 */
void updateBilling(char e_billing[]);

/**
 * @brief copies the tod energy from the "e_tod_energy_kwh" variable into the local "tod_energy_kwh" variable.
 * @param e_tod_energy_kwh		- 			e_tod_energy_kwh parameter holding tod energy.
 * @returns void.
 */
void updateTodEnergyKWh(float e_tod_energy_kwh);

/**
 * @brief copies the tod max apparent power demand from the "e_tod_max_demand_kva" variable into the local "tod_max_demand_kva" variable.
 * @param e_tod_max_demand_kva	- 			e_tod_max_demand_kva parameter holding tod max apparent power demand.
 * @returns void.
 */
void updateTodMaxDemandKVA(float e_tod_max_demand_kva);

/**
 * @brief copies the cumulative max demand from the "e_cumul_max_demand" variable into the local "cumul_max_demand" variable.
 * @param e_cumul_max_demand		- 			e_cumul_max_demand parameter holding cumulative max demand.
 * @returns void.
 */
void updateCumulMaxDemand(float e_cumul_max_demand);

/**
 * @brief copies the tamper switch status from the "e_tamper_switch_stat" variable into the local "tamper_switch_stat" variable.
 * @param e_tamper_switch_stat	- 			e_tamper_switch_stat parameter holding tamper switch status.
 * @returns void.
 */
void updateTamperSwitchStat(char e_tamper_switch_stat[]);

/**
 * @brief copies the last occurrence of abnormality from the "e_last_of_occur_of_abn" parameter into the local "last_of_occur_of_abn" variable.
 * @param e_last_of_occur_of_abn	- 			e_last_of_occur_of_abn parameter holding last occurrence of abnormality.
 * @returns void.
 */
void updateLastOccurOfAbn(char e_last_of_occur_of_abn[]);

/**
 * @brief copies the latest restoration of abnormality from the "e_ltst_restn_of_abn" parameter into the local "ltst_restn_of_abn" variable.
 * @param e_ltst_restn_of_abn		- 			e_ltst_restn_of_abn parameter holding latest restoration of abnormality.
 * @returns void.
 */
void updateLtstRestnOfAbn(char e_ltst_restn_of_abn[]);


void initializeParameterN(parameter * param);


void initializeParameterS(parameter * param);

void setTenant(int t);

int getTenant();

void resetTenant();

void incrementTenant();

parameter getParameter(uint8_t param_num);
#endif /* LCD_DRIVER_INC_LCD_DRIVER_H_ */
