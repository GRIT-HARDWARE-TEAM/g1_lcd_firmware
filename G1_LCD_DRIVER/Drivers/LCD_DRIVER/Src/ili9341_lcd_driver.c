/*
 * ili9341_lcd_driver.c
 * ILI9341_LCD_DRIVER_INTERFACE
 * This relies on low level functions written by Matej Artnak.
 * Thanks Matej!.
 * Created on: 31 Oct 2018
 * Author: Mubarak Okunade
 *
 */

#include "lcd_driver.h"
#include "string.h"
#include "math.h"

#if ILI9341 == 1
	#include "ILI9341_GFX.h"
	#include "ILI9341_STM32_Driver.h"
	#include "ILI9341_Touchscreen.h"

	icon over_voltage_icon,over_current_icon,tamper_switch_icon,credit_status_icon;


	/**
	 * @brief parameter variables.
	 * @brief These are the actual values that are displayed on the screen.
	 * 		  They can be used in any part of the project.
	 * 		  To 'get' the values they hold use their corresponding 'get' functions.
	 * 		  To 'update' them just use their corresponding 'update' functions.
	 */
	parameter 	lcd_test_txt, date,real_time, active_energy,
				reactive_energy, apparent_energy,
				max_demand_kw, max_demand_kva, md_reset_count,
				inst_pf, inst_freq, inst_voltage, inst_current,
				inst_kw, inst_kvar, inst_kva,billing, tod_energy_kwh,
				tod_max_demand_kva, cumul_max_demand,
				last_occur_of_abn, ltst_restn_of_abn,
				error_parameter;


				//tamper_switch_stat;

	int tenant = 0;
	//uint8_t counter = 0;
	bool drawOnce = true;
	bool drawBackground = true;
	bool buttonPressedBefore = false;

	// Has to be MAX_NO_OF_TENANTS - 1 because of 0-indexing

	void setTenant(int t){
		if(t > MAX_NO_OF_TENANTS - 1){
			resetTenant();
		}
		else{
			tenant = t;
		}
	}

	int getTenant(){
		if(tenant > MAX_NO_OF_TENANTS - 1){
			resetTenant();
		}
		return tenant;
	}

	void resetTenant(){
		tenant = 0;
	}

	void incrementTenant(){
		//if(tenant > MAX_NO_OF_TENANTS - 1){
		if(tenant >= MAX_NO_OF_TENANTS - 1){
			resetTenant();
		}
		else{
			tenant = tenant + 1;
		}
	}

	void initializeParameterN(parameter * param){
		param->valueType = 1;
		param->fontSelector = 1;
		param->nValue = 0;
		sprintf(param->sValue,"%s","");
		param->standAloneStatus = 0;
		param->updateFlag = true;
	}

	void initializeParameterS(parameter * param){
		param->valueType = 0;
		param->fontSelector = 0;
		param->nValue = 0;
		sprintf(param->sValue,"%s","");
		param->standAloneStatus = 0;
		param->updateFlag = true;
	}

	void initializeParameters(){

		sprintf(lcd_test_txt.name,"LCD_TEST");
		lcd_test_txt.valueType = 0;
		lcd_test_txt.fontSelector = 0;
		lcd_test_txt.nValue = 0;
		sprintf(lcd_test_txt.sValue,"NULL");
		lcd_test_txt.standAloneStatus = 1;
		lcd_test_txt.updateFlag = true;
		//initializeParameterS(&lcd_test_txt);

		sprintf(date.name,"DATE");
		date.valueType = 0;
		date.fontSelector = 1;
		date.nValue = 0;
		sprintf(date.sValue,"240119");
		date.standAloneStatus = 0;
		date.updateFlag = true;

		sprintf(real_time.name,"REAL TIME");
		real_time.valueType = 0;
		real_time.fontSelector = 1;
		real_time.nValue = 0;
		sprintf(real_time.sValue,"10:27");
		real_time.standAloneStatus = 0;
		real_time.updateFlag = true;

		sprintf(active_energy.name,"ACTIVE ENERGY");
		active_energy.valueType = 1;
		active_energy.fontSelector = 1;
		active_energy.nValue = 1.23456;
		sprintf(active_energy.sValue,"%s","");
		active_energy.standAloneStatus = 0;
		active_energy.updateFlag = true;

		sprintf(reactive_energy.name,"REACTIVE ENERGY");
		reactive_energy.valueType = 1;
		reactive_energy.fontSelector = 1;
		reactive_energy.nValue = 12.3456;
		sprintf(reactive_energy.sValue,"%s","");
		reactive_energy.standAloneStatus = 0;
		reactive_energy.updateFlag = true;

		sprintf(apparent_energy.name,"APPARENT ENERGY");
		apparent_energy.valueType = 1;
		apparent_energy.fontSelector = 1;
		apparent_energy.nValue = 123.456;
		sprintf(apparent_energy.sValue,"%s","");
		apparent_energy.standAloneStatus = 0;
		apparent_energy.updateFlag = true;

		sprintf(max_demand_kw.name,"MAX. DEMAND(KW)");
		max_demand_kw.valueType = 1;
		max_demand_kw.fontSelector = 1;
		max_demand_kw.nValue = 1234.56;
		sprintf(max_demand_kw.sValue,"%s","");
		max_demand_kw.standAloneStatus = 0;
		max_demand_kw.updateFlag = true;

		sprintf(max_demand_kva.name,"MAX. DEMAND(KVA)");
		max_demand_kva.valueType = 1;
		max_demand_kva.fontSelector = 1;
		max_demand_kva.nValue = 12345.6;
		sprintf(max_demand_kva.sValue,"%s","");
		max_demand_kva.standAloneStatus = 0;
		max_demand_kva.updateFlag = true;

		sprintf(md_reset_count.name,"MD RESET COUNT");
		md_reset_count.valueType = 1;
		md_reset_count.fontSelector = 1;
		md_reset_count.nValue = 26;
		sprintf(md_reset_count.sValue,"%s","");
		md_reset_count.standAloneStatus = 0;
		md_reset_count.updateFlag = true;

		sprintf(inst_pf.name,"INST. PF");
		inst_pf.valueType = 1;
		inst_pf.fontSelector = 1;
		inst_pf.nValue = 0.7;
		sprintf(inst_pf.sValue,"%s","");
		inst_pf.standAloneStatus = 0;
		inst_pf.updateFlag = true;

		sprintf(inst_freq.name,"INST. FREQ(HZ)");
		inst_freq.valueType = 1;
		inst_freq.fontSelector = 1;
		inst_freq.nValue = 48;
		sprintf(inst_freq.sValue,"%s","");
		inst_freq.standAloneStatus = 0;
		inst_freq.updateFlag = true;

		sprintf(inst_voltage.name,"INST. VOLTAGE(V)");
		inst_voltage.valueType = 1;
		inst_voltage.fontSelector = 1;
		inst_voltage.nValue = 230;
		sprintf(inst_voltage.sValue,"%s","");
		inst_voltage.standAloneStatus = 0;
		inst_voltage.updateFlag = true;

		sprintf(inst_current.name,"INST. CURRENT(A)");
		inst_current.valueType = 1;
		inst_current.fontSelector = 1;
		inst_current.nValue = 1.5;
		sprintf(inst_current.sValue,"%s","");
		inst_current.standAloneStatus = 0;
		inst_current.updateFlag = true;

		sprintf(inst_kw.name,"INST. KW");
		inst_kw.valueType = 1;
		inst_kw.fontSelector = 1;
		inst_kw.nValue = 2.0;
		sprintf(inst_kw.sValue,"%s","");
		inst_kw.standAloneStatus = 0;
		inst_kw.updateFlag = true;

		sprintf(inst_kvar.name,"INST. KVAr");
		inst_kvar.valueType = 1;
		inst_kvar.fontSelector = 1;
		inst_kvar.nValue = 1.23456;
		sprintf(inst_kvar.sValue,"%s","");
		inst_kvar.standAloneStatus = 0;
		inst_kvar.updateFlag = true;

		sprintf(inst_kva.name,"INST. KVA");
		inst_kva.valueType = 1;
		inst_kva.fontSelector = 1;
		inst_kva.nValue = 2.0;
		sprintf(inst_kva.sValue,"%s","");
		inst_kva.standAloneStatus = 0;
		inst_kva.updateFlag = true;

		sprintf(billing.name,"BILLING(KWh)");
		billing.valueType = 1;
		billing.fontSelector = 1;
		billing.nValue = 120;
		sprintf(billing.sValue,"NULL");
		billing.standAloneStatus = 0;
		billing.updateFlag = true;
		//initializeParameterS(&billing);

		sprintf(tod_energy_kwh.name,"TOD energy(KWh)");
		tod_energy_kwh.valueType = 1;
		tod_energy_kwh.fontSelector = 1;
		tod_energy_kwh.nValue = 100;
		sprintf(tod_energy_kwh.sValue,"%s","");
		tod_energy_kwh.standAloneStatus = 0;
		tod_energy_kwh.updateFlag = true;
		//initializeParameterN(&tod_energy_kwh);

		sprintf(tod_max_demand_kva.name,"TOD Max demand(KVA)");
		tod_max_demand_kva.valueType = 1;
		tod_max_demand_kva.fontSelector = 1;
		tod_max_demand_kva.nValue = 5;
		sprintf(tod_max_demand_kva.sValue,"%s","");
		tod_max_demand_kva.standAloneStatus = 0;
		tod_max_demand_kva.updateFlag = true;
		//initializeParameterN(&tod_max_demand_kva);

		sprintf(cumul_max_demand.name,"CUMUL. MAX DEMAND");
		cumul_max_demand.valueType = 1;
		cumul_max_demand.fontSelector = 1;
		cumul_max_demand.nValue = 5;
		sprintf(cumul_max_demand.sValue,"%s","");
		cumul_max_demand.standAloneStatus = 0;
		cumul_max_demand.updateFlag = true;
		//initializeParameterN(&cumul_max_demand);


		/*
		tamper_switch_icon.stateTxt.valueType = 0;
		tamper_switch_icon.stateTxt.nValue = 0;
		sprintf(tamper_switch_icon.stateTxt.sValue,"NULL");
		tamper_switch_icon.stateTxt.standAloneStatus = 0;
		*/

		sprintf(last_occur_of_abn.name,"LAST OCCRNC. OF ABN.");
		last_occur_of_abn.valueType = 0;
		last_occur_of_abn.fontSelector = 1;
		last_occur_of_abn.nValue = 0;
		sprintf(last_occur_of_abn.sValue,"NULL");
		last_occur_of_abn.standAloneStatus = 0;
		last_occur_of_abn.updateFlag = true;
		//initializeParameterS(&last_occur_of_abn);

		sprintf(ltst_restn_of_abn.name,"LTST RESTN. OF ABN.");
		ltst_restn_of_abn.valueType = 0;
		ltst_restn_of_abn.fontSelector = 1;
		ltst_restn_of_abn.nValue = 0;
		sprintf(ltst_restn_of_abn.sValue,"NULL");
		ltst_restn_of_abn.standAloneStatus = 0;
		ltst_restn_of_abn.updateFlag = true;
		//initializeParameterS(&ltst_restn_of_abn);

		sprintf(error_parameter.name,"ERROR!!!.");
		error_parameter.valueType = 0;
		error_parameter.fontSelector = 0;
		error_parameter.nValue = 0;
		sprintf(error_parameter.sValue,"ERROR!!!");
		error_parameter.standAloneStatus = 0;
		error_parameter.updateFlag = true;
	}


	void initializeIcons(){
		sprintf(over_voltage_icon.symbolChar,OVER_VOLTAGE_FAULT_SYMBOL);
		over_voltage_icon.x = OVER_VOLTAGE_FAULT_SYMBOL_X_COORD;
		over_voltage_icon.y = OVER_VOLTAGE_FAULT_SYMBOL_Y_COORD;
		over_voltage_icon.state = OK;

		sprintf(over_voltage_icon.stateTxt.name,"OV ICON PARAM");
		over_voltage_icon.stateTxt.valueType = 0;
		over_voltage_icon.stateTxt.nValue = 0;
		setIconParameterProperty(&over_voltage_icon);
		over_voltage_icon.stateTxt.standAloneStatus = 0;


		sprintf(over_current_icon.symbolChar,OVER_CURRENT_FAULT_SYMBOL);
		over_current_icon.x = OVER_CURRENT_FAULT_SYMBOL_X_COORD;
		over_current_icon.y = OVER_CURRENT_FAULT_SYMBOL_Y_COORD;
		over_current_icon.state = OK;

		sprintf(over_current_icon.stateTxt.name,"OC ICON PARAM");
		over_current_icon.stateTxt.valueType = 0;
		over_current_icon.stateTxt.nValue = 0;
		setIconParameterProperty(&over_current_icon);
		over_current_icon.stateTxt.standAloneStatus = 0;


		sprintf(tamper_switch_icon.symbolChar,TAMPER_SWITCH_STATUS_SYMBOL);
		tamper_switch_icon.x = TAMPER_SWITCH_STATUS_SYMBOL_X_COORD;
		tamper_switch_icon.y = TAMPER_SWITCH_STATUS_SYMBOL_Y_COORD;
		tamper_switch_icon.state = OK;

		sprintf(tamper_switch_icon.stateTxt.name,"TS ICON PARAM");
		tamper_switch_icon.stateTxt.valueType = 0;
		tamper_switch_icon.stateTxt.nValue = 0;
		setIconParameterProperty(&tamper_switch_icon);
		tamper_switch_icon.stateTxt.standAloneStatus = 0;


		sprintf(credit_status_icon.symbolChar,CREDIT_STATUS_SYMBOL);
		credit_status_icon.x = CREDIT_STATUS_SYMBOL_X_COORD;
		credit_status_icon.y = CREDIT_STATUS_SYMBOL_Y_COORD;
		credit_status_icon.state = OK;

		sprintf(credit_status_icon.stateTxt.name,"CS ICON PARAM");
		credit_status_icon.stateTxt.valueType = 0;
		credit_status_icon.stateTxt.nValue = 0;
		setIconParameterProperty(&credit_status_icon);
		credit_status_icon.stateTxt.standAloneStatus = 0;
	}

	void updateIconState(icon statusIcon, iconState statusIconState){
		statusIcon.state = statusIconState;
	}

	void updateOVIconState(iconState statusIconState){
		over_voltage_icon.state = statusIconState;
		setIconParameterProperty(&over_voltage_icon);
	}

	void updateOCIconState(iconState statusIconState){
		over_current_icon.state = statusIconState;
		setIconParameterProperty(&over_current_icon);
	}

	void updateTSIconState(iconState statusIconState){
		tamper_switch_icon.state = statusIconState;
		setIconParameterProperty(&tamper_switch_icon);
	}

	void updateCSIconState(iconState statusIconState){
		credit_status_icon.state = statusIconState;
		setIconParameterProperty(&credit_status_icon);
	}

	void floatToString(float number, char string[]){
		//snprintf(string,8,"%f",number);
		//sprintf(string,"%f",number);
		//strcpy(string,"TEST");
		gcvt(number,6,string);	// clearly this does it well!
		//printf(string);

		// handles decimal numbers from 0 to 6dp
		/*
		int wholeNumber = number;
		float dummyDigits = 0.012;
		double fraction = number - wholeNumber;

		//int convertedFraction = trunc(fraction * 100000);
		//sprintf(string,"%d.%05d",wholeNumber,convertedFraction);

		// make it flexible for 0 to 6dp
		if(wholeNumber <= 9  ){
			int convertedFraction = trunc(fraction * 100000);
			sprintf(string,"%d.%05d",wholeNumber,convertedFraction);
		}
		else if(wholeNumber <= 99  ){
			int convertedFraction = trunc(fraction * 10000);
			sprintf(string,"%d.%04d",wholeNumber,convertedFraction);
		}
		else if(wholeNumber <= 999  ){
			int convertedFraction = trunc(fraction * 1000);
			sprintf(string,"%d.%03d",wholeNumber,convertedFraction);
		}
		else if(wholeNumber <= 9999  ){
			int convertedFraction = trunc(fraction * 100);
			sprintf(string,"%d.%02d",wholeNumber,convertedFraction);
		}
		else if(wholeNumber <= 99999  ){
			int convertedFraction = trunc(fraction * 10);
			sprintf(string,"%d.%01d",wholeNumber,convertedFraction);
		}
		else{
			//int convertedFraction = trunc(fraction * 100000);
			sprintf(string,"%d",wholeNumber);
		}*/
	}

	void backlight(bool on){
		if(on){
			dimBacklight(BACKLIGHT_DIMMER_ON);
		}
		else{
			setBrightnessPWM(0);
		}
	}

	void dimBacklight(bool status){
		if(status == true){
			setBrightnessPWM(DIM_BRIGHTNESS_SETTING);
		}
		else{
			setBrightnessPWM(MAX_BRIGHTNESS_SETTING);
		}
	}

	void setBrightnessPWM(uint8_t brightness){
		// brightness param is from 0 - 100
		TIM_OC_InitTypeDef sConfigOC;
		sConfigOC.OCMode = TIM_OCMODE_PWM1;
		sConfigOC.Pulse = 100 - brightness;
		sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
		sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
		HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1);
		HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);

	};

	void clear(){
		ILI9341_Fill_Screen(BACKGROUND_COLOR);
	}

	void setup(){
		ILI9341_Init();
		clear();
		ILI9341_Set_Rotation(SCREEN_HORIZONTAL_1);
		initializeIcons();
		initializeParameters();
		backlight(true);
		drawPage();
	}

	void print(const char* text, uint16_t x, uint16_t y, uint16_t colour, uint16_t size, uint16_t background_colour){
		ILI9341_Draw_Text(text,x,y,colour,size,background_colour);
	}

	void printn(const char* text, uint16_t x, uint16_t y, uint16_t colour, uint16_t size, uint16_t background_colour){
		ILI9341_Draw_Textn(text,x,y,colour,size,background_colour);
	}

	void printServiceMessage(const char* text, uint16_t x , uint16_t y){
		print(text,x,y,TEXT_COLOR,SMALL_FONT_SIZE,BACKGROUND_COLOR);
	}

	void printServiceMessages(serviceMessage service_messages[], uint8_t n,uint16_t * serviceMessageDelay){
		if(n <= 0){return ;}
		drawMessageBox();
		uint16_t maxDelay = 0;	// largest service message delay property.
		for(int i = 0; i < n; i++){
			serviceMessage service_message = service_messages[i];
			//print(service_message.message,service_message.x,service_message.y,TEXT_COLOR,SMALL_FONT_SIZE,BACKGROUND_COLOR);
			printServiceMessage(service_message.message,service_message.x,service_message.y);
			if(service_message.delay > maxDelay){
				maxDelay = service_message.delay;
			}
		}
		* serviceMessageDelay = maxDelay;	// means for an array of service messages, the largest delay property will be selected
	}

	//void printParameterName(intmax_t parameter_number,uint8_t section_number){
	void printParameterName(parameter param,uint8_t section_number){
		/*char ten_ind[3] = {"T1\0"};
		switch (tenant) {
			case 0:
				sprintf(ten_ind, " T1");
				break;
			case 1:
				sprintf(ten_ind," T2");
				break;
			case 2:
				sprintf(ten_ind," T3");
				break;
			case 3:
				sprintf(ten_ind," T4");
				break;
			default:
				sprintf(ten_ind," ER");
				break;
		}
		char temp[31] = "";*/
		//sprintf(temp,PARAMETER_LIST[parameter_number]);
		/* This makes multi tenant support smarter.
		 * It will only print "Tn" for values
		 * that are different for each tenant.
		 * */
		/*
		if(param.valueType == 1){
			sprintf(temp,param.name);
			strcat(temp,ten_ind);
		}
		else{
			sprintf(temp,param.name);
		}*/
		print(param.name,PARAM_X_COORD,PARAM_Y_COORD+(SECTION_DISPLACEMENT*section_number),TEXT_COLOR,SMALL_FONT_SIZE,BACKGROUND_COLOR);
	}

	void getLCDTestTxt(char data[]){
		strcpy(data,lcd_test_txt.sValue);
		strcat(data,"\0");
	}

	void getDate(char data[]){
		strcpy(data,date.sValue);
		strcat(data,"\0");
	}
	void getRealTime(char data[]){
		strcpy(data,real_time.sValue);
		strcat(data,"\0");
	}
	void getActiveEnergy(char data[]){

		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(active_energy.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}

	}
	void getReactiveEnergy(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(reactive_energy.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}

	}
	void getApparentEnergy(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(apparent_energy.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}

	}
	void getMaxDemandKW(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(max_demand_kw.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}

	}
	void getMaxDemandKVA(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(max_demand_kva.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}

	}
	void getMDResetCount(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			sprintf(data,"%u",(unsigned int)md_reset_count.nValue);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}
	}
	void getInstPF(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(inst_pf.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}
	}
	void getInstFreq(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(inst_freq.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}
	}
	void getInstVoltage(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(inst_voltage.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}
	}
	void getInstCurrent(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(inst_current.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}

	}
	void getInstKW(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(inst_kw.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}

	}
	void getInstKVAr(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(inst_kvar.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}
	}
	void getInstKVA(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(inst_kva.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}

	}
	void getBilling(char data[]){
		strcpy(data,billing.sValue);
		strcat(data,"\0");
	}
	void getTodEnergyKWh(char data[]){
		floatToString(tod_energy_kwh.nValue,data);
		strcat(data,"\0");
	}
	void getTodMaxDemandKVA(char data[]){
		floatToString(tod_max_demand_kva.nValue,data);
		strcat(data,"\0");
	}
	void getCumulMaxDemand(char data[]){
		if(tenant < MAX_NO_OF_TENANTS){
			floatToString(cumul_max_demand.nValue,data);
			strcat(data,"\0");
		}
		else{
			strcpy(data,"NULL");
			strcat(data,"\0");
		}
	}
	void getTamperSwitchStat(char data[]){
		strcpy(data,tamper_switch_icon.stateTxt.sValue);
		//strcpy(data,"HELLO");
		strcat(data,"\0");
	}
	void getLastOccurOfAbn(char data[]){
		strcpy(data,last_occur_of_abn.sValue);
		strcat(data,"\0");
	}
	void getLtstRestnOfAbn(char data[]){
		strcpy(data,ltst_restn_of_abn.sValue);
		strcat(data,"\0");
	}

	void get(intmax_t parameter_number,char data[], parameter * param){
		switch (parameter_number) {
			case 0:
				//strcpy(data,"NULL\0");
				getLCDTestTxt(data);
				* param = lcd_test_txt;
				lcd_test_txt.updateFlag = false;
				break;
			case 1:
				getDate(data);
				* param = date;
				date.updateFlag = false;
				break;
			case 2:
				getRealTime(data);
				* param = real_time;
				real_time.updateFlag = false;
				break;

			case 3:
				getActiveEnergy(data);
				* param = active_energy;
				active_energy.updateFlag = false;
				break;

			case 4:
				getInstKW(data);
				* param = inst_kw;
				inst_kw.updateFlag = false;
				break;

			case 5:
				getReactiveEnergy(data);
				* param = reactive_energy;
				reactive_energy.updateFlag = false;
				break;

			case 6:
				getInstKVAr(data);
				* param = inst_kvar;
				inst_kvar.updateFlag = false;
				break;

			case 7:
				getApparentEnergy(data);
				* param = apparent_energy;
				apparent_energy.updateFlag = false;
				break;

			case 8:
				getInstKVA(data);
				* param = inst_kva;
				inst_kva.updateFlag = false;
				break;
			case 9:
				getMaxDemandKW(data);
				* param = max_demand_kw;
				max_demand_kw.updateFlag = false;
				break;
			case 10:
				getMaxDemandKVA(data);
				* param = max_demand_kva;
				max_demand_kva.updateFlag = false;
				break;
			case 11:
				getMDResetCount(data);
				* param = md_reset_count;
				md_reset_count.updateFlag = false;
				break;
			case 12:
				getInstPF(data);
				* param = inst_pf;
				inst_pf.updateFlag = false;
				break;
			case 13:
				getInstFreq(data);
				* param = inst_freq;
				inst_freq.updateFlag = false;
				break;
			case 14:
				getInstVoltage(data);
				* param = inst_voltage;
				inst_voltage.updateFlag = false;
				break;
			case 15:
				getInstCurrent(data);
				* param = inst_current;
				inst_current.updateFlag = false;
				break;
			case 16:
				getBilling(data);
				* param = billing;
				billing.updateFlag = false;
				break;
			case 17:
				getTodEnergyKWh(data);
				* param = tod_energy_kwh;
				tod_energy_kwh.updateFlag = false;
				break;
			case 18:
				getTodMaxDemandKVA(data);
				* param = tod_max_demand_kva;
				tod_max_demand_kva.updateFlag = false;
				break;
			case 19:
				getCumulMaxDemand(data);
				* param = cumul_max_demand;
				cumul_max_demand.updateFlag = false;
				break;
			case 20:
				getTamperSwitchStat(data);
				* param = tamper_switch_icon.stateTxt;
				//tamper_switch_icon.stateTxt.updateFlag = false;
				/*
				 * Please do not mess with case 20!.
				 * It must be this way so the text version and icon version of the tamper switch status
				 * will be displayed and updated quasi-simultaneously.
				 *
				 * */
				break;
			case 21:
				getLastOccurOfAbn(data);
				* param = last_occur_of_abn;
				last_occur_of_abn.updateFlag = false;
				break;
			case 22:
				getLtstRestnOfAbn(data);
				* param = ltst_restn_of_abn;
				ltst_restn_of_abn.updateFlag = false;
				break;
			default:
				data = "ERROR!!!\0";
				sprintf(param->sValue,"ERROR!!!");
				break;
		}
	}


	//void printParameterValue(intmax_t parameter_number,uint8_t section_number){
	void printParameterValue(parameter param,uint8_t section_number){
		//print(MENU_PAGES[page_number][ITEM_INDEX],5,70,TEXT_COLOR,BIG_FONT_SIZE,BACKGROUND_COLOR);
		char data[24] = "DEFAULT\0";
		//parameter param;
		//initializeParameterN(&param);
		//get(parameter_number,data,&param);

		if(param.updateFlag == true){
			if(param.fontSelector == 1){
				if(param.valueType == 1){
					floatToString(param.nValue,data);
				}
				else{
					strcpy(data,param.sValue);
				}
				printn(data,PARAM_VAL_X_COORD,PARAM_VAL_Y_COORD+(SECTION_DISPLACEMENT*section_number),TEXT_COLOR,BIG_FONT_SIZE,BACKGROUND_COLOR);
			}
			else{
				if(param.valueType == 1){
					floatToString(param.nValue,data);
				}
				else{
					strcpy(data,param.sValue);
				}
				print(data,PARAM_VAL_X_COORD,PARAM_VAL_Y_COORD+(SECTION_DISPLACEMENT*section_number),TEXT_COLOR,MEDIUM_FONT_SIZE,BACKGROUND_COLOR);
			}
		}
	}


	void printDefaultPage(bool buttonPress){

		// hard coded.
		// 19 => parameter 19 => cumulative kwh
		// 0 => section 0 => print at the top section of the page
		parameter param = getParameter(19);
		if(drawBackground){
			ILI9341_Draw_Filled_Rectangle_Coord(PAGE_BORDER_X0+1,PAGE_BORDER_Y0+PAGE_TITLE_THICKNESS+1,PAGE_BORDER_X1-1,PAGE_BORDER_Y1-1,BACKGROUND_COLOR);
			printParameterName(param,0);
		}

		if(buttonPress){
			drawBackground = true;
			buttonPressedBefore = true;
		}
		else{
			drawBackground = false;
		}

		printParameterValue(param,0);
		drawIconSection();
	}
	intmax_t prev_param_num = -1;	// keeps track of the previous page selected.
	void printMenuPage(intmax_t * parameter_number){
		intmax_t param_num = (intmax_t) * parameter_number;

		// what was going through my mind???
		if(param_num > (NO_OF_PARAMETERS - 1)){
			// for positive numbers that exceed the number of parameters.
			//param_num = param_num%(NO_OF_PARAMETERS + 1);
			param_num = param_num - NO_OF_PARAMETERS;
			* parameter_number = (intmax_t)param_num;
		}
		else if(param_num < 0){
			// for negative numbers that occur as a result of going backwards.
			//param_num = NO_OF_PARAMETERS + 1 + param_num;
			param_num = NO_OF_PARAMETERS + param_num;
			* parameter_number = (intmax_t)param_num;
		}
		// if its printing a new page
		if(prev_param_num != param_num){
			ILI9341_Draw_Filled_Rectangle_Coord(PAGE_BORDER_X0+1,PAGE_BORDER_Y0+PAGE_TITLE_THICKNESS+1,PAGE_BORDER_X1-1,PAGE_BORDER_Y1-1,BACKGROUND_COLOR);
		}
		uint8_t section_number = 0;
		// get the parameter structure for both printParameterName() and printParameterValue()
		parameter param = getParameter(param_num);
		// pass parameter structure into the aforementioned functions

		if(prev_param_num != param_num){
			// if its printing a new page
			printParameterName(param,section_number);

		}


		//printParameterName(param,section_number);
		printParameterValue(param,section_number);
		// This ensures that the LCD test values are alone on a page.
		if(param_num != 0){
			++section_number;
			++param_num;
			param = getParameter(param_num);
			// intent of this if statement ??????
			/*
			if(param_num < NO_OF_PARAMETERS-1){
				if(prev_param_num != param_num-1){
					printParameterName(param_num,section_number);
				}
				printParameterValue(param_num,section_number);
			}*/
			if((param_num < NO_OF_PARAMETERS)){
				// commented out so it can print from t1 ... t4.

				if(prev_param_num != param_num-1){
					printParameterName(param,section_number);
				}

				//printParameterName(param,section_number);
				printParameterValue(param,section_number);
			}
			prev_param_num = param_num-1;
		}
		else{
			prev_param_num = param_num;
		}
		drawIconSection();
	}

	void drawPage(){
		ILI9341_Draw_Hollow_Rectangle_Coord(PAGE_BORDER_X0,PAGE_BORDER_Y0,PAGE_BORDER_X1,PAGE_BORDER_Y1,TEXT_COLOR);
		ILI9341_Draw_Horizontal_Line(PAGE_TITLE_LINE_X_COORD,PAGE_TITLE_LINE_Y_COORD,PAGE_TITLE_LINE_LENGTH,TEXT_COLOR);
		print(PAGE_TITLE, 2 + PAGE_TITLE_TAB_LENGTH, 2, TEXT_COLOR, SMALL_FONT_SIZE, BACKGROUND_COLOR);
	}

	void drawMessageBox(){
		ILI9341_Draw_Filled_Rectangle_Coord(MSGBOX_BORDER_X0,MSGBOX_BORDER_Y0,MSGBOX_BORDER_X1,MSGBOX_BORDER_Y1,BACKGROUND_COLOR);
		ILI9341_Draw_Hollow_Rectangle_Coord(MSGBOX_BORDER_X0,MSGBOX_BORDER_Y0,MSGBOX_BORDER_X1,MSGBOX_BORDER_Y1,TEXT_COLOR);
		ILI9341_Draw_Horizontal_Line(MSGBOX_TITLE_LINE_X_COORD,MSGBOX_TITLE_LINE_Y_COORD,MSGBOX_TITLE_LINE_LENGTH,TEXT_COLOR);
		print(MSGBOX_TITLE, MSGBOX_BORDER_X0 + 2 + MSGBOX_TITLE_TAB_LENGTH, MSGBOX_BORDER_Y0 + 2, TEXT_COLOR, SMALL_FONT_SIZE, BACKGROUND_COLOR);
	}

	void clearServiceMessageBox(){
		ILI9341_Draw_Filled_Rectangle_Coord(MSGBOX_BORDER_X0,MSGBOX_BORDER_Y0,MSGBOX_BORDER_X1+1,MSGBOX_BORDER_Y1+1,BACKGROUND_COLOR);
	}

	void drawIcon(char symbolChars[], uint16_t x, uint16_t y,uint16_t color){
		print(symbolChars,x,y,color,ICON_SIZE,BACKGROUND_COLOR);
	}

	uint16_t getIconColorFromState(iconState state){
		uint16_t color = 0;
		switch (state) {
			case OK:
				color = GREEN;
				break;
			case NOT_OK:
				color = RED;
				break;
			case OVER_VOLTAGE_FAULT:
				color = RED;
				break;
			case OVER_CURRENT_FAULT:
				color = RED;
				break;
			case MAGNETIC_TAMPER_EVENT:
				color = ORANGE;
				break;
			case CASE_TAMPER_EVENT:
				color = RED;
				break;
			default:
				color = LIGHTGREY;
		}
		return color;
	}

	void setIconParameterProperty(icon * icon){
		icon->stateTxt.updateFlag = true;
		switch (icon->state) {
			case OK:
				sprintf(icon->stateTxt.sValue,"OK");
				break;
			case NOT_OK:
				sprintf(icon->stateTxt.sValue,"NOT OK");
				break;
			case OVER_VOLTAGE_FAULT:
				sprintf(icon->stateTxt.sValue,"OVER VOLTAGE FAULT");
				break;
			case OVER_CURRENT_FAULT:
				sprintf(icon->stateTxt.sValue,"OVER CURRENT FAULT");
				break;
			case MAGNETIC_TAMPER_EVENT:
				sprintf(icon->stateTxt.sValue,"MAGN TMP");
				//strcpy(icon.stateTxt.sValue,"MAGN TMP\0");
				break;
			case CASE_TAMPER_EVENT:
				sprintf(icon->stateTxt.sValue,"CASE TMP");
				break;
			default:
				sprintf(icon->stateTxt.sValue,"ERROR!");
		}
	}

	void drawIconSection(){
		if(over_voltage_icon.stateTxt.updateFlag){
			drawIcon(over_voltage_icon.symbolChar,over_voltage_icon.x,over_voltage_icon.y,getIconColorFromState(over_voltage_icon.state));
			over_voltage_icon.stateTxt.updateFlag = false;
		}

		if(over_current_icon.stateTxt.updateFlag){
			drawIcon(over_current_icon.symbolChar,over_current_icon.x,over_current_icon.y,getIconColorFromState(over_current_icon.state));
			over_current_icon.stateTxt.updateFlag = false;
		}

		if(tamper_switch_icon.stateTxt.updateFlag){
			drawIcon(tamper_switch_icon.symbolChar,tamper_switch_icon.x,tamper_switch_icon.y,getIconColorFromState(tamper_switch_icon.state));
			tamper_switch_icon.stateTxt.updateFlag = false;
		}

		if(credit_status_icon.stateTxt.updateFlag){
			drawIcon(credit_status_icon.symbolChar,credit_status_icon.x,credit_status_icon.y,getIconColorFromState(credit_status_icon.state));
			credit_status_icon.stateTxt.updateFlag = false;
		}

	}


	void updateLCDTestTxt(char e_lcd_test_txt[]){
		strcpy(lcd_test_txt.sValue,e_lcd_test_txt);
		strcat(lcd_test_txt.sValue,"\0");
		lcd_test_txt.updateFlag = true;
	}
	void updateDate(char e_date[]){
		strcpy(date.sValue,e_date);
		strcat(date.sValue,"\0");
		date.updateFlag = true;
	}
	void updateRealTime(char e_real_time[]){
		strcpy(real_time.sValue,e_real_time);
		strcat(real_time.sValue,"\0");
		real_time.updateFlag = true;
	}
	void updateActiveEnergy(float e_active_energy){
		active_energy.nValue = e_active_energy;
		active_energy.updateFlag = true;
	}
	void updateReactiveEnergy(float e_reactive_energy){
		reactive_energy.nValue = e_reactive_energy;
		reactive_energy.updateFlag = true;
	}
	void updateApparentEnergy(float e_apparent_energy){
		apparent_energy.nValue = e_apparent_energy;
		apparent_energy.updateFlag = true;
	}
	void updateMaxDemandKW(float e_max_demand_kw){
		max_demand_kw.nValue = e_max_demand_kw;
		max_demand_kw.updateFlag = true;
	}
	void updateMaxDemandKVA(float e_max_demand_kva){
		max_demand_kva.nValue = e_max_demand_kva;
		max_demand_kva.updateFlag = true;
	}
	void updateMDResetCount(unsigned int e_md_reset_count){
		md_reset_count.nValue = e_md_reset_count;
		md_reset_count.updateFlag = true;
	}
	void updateInstPF(float e_inst_pf){
		inst_pf.nValue = e_inst_pf;
		inst_pf.updateFlag = true;
	}
	void updateInstFreq(float e_inst_freq){
		inst_freq.nValue = e_inst_freq;
		inst_freq.updateFlag = true;
	}
	void updateInstVoltage(float e_inst_voltage){
		inst_voltage.nValue = e_inst_voltage;
		inst_voltage.updateFlag = true;
	}
	void updateInstCurrent(float e_inst_current){
		inst_current.nValue = e_inst_current;
		inst_current.updateFlag = true;
	}
	void updateInstKW(float e_inst_kw){
		inst_kw.nValue = e_inst_kw;
		inst_kw.updateFlag = true;
	}
	void updateInstKVAr(float e_inst_kvar){
		inst_kvar.nValue = e_inst_kvar;
		inst_kvar.updateFlag = true;
	}
	void updateInstKVA(float e_inst_kva){
		inst_kva.nValue = e_inst_kva;
		inst_kva.updateFlag = true;
	}
	void updateBilling(char e_billing[]){
		strcpy(billing.sValue,e_billing);
		strcat(billing.sValue,"\0");
		billing.updateFlag = true;
	}
	void updateTodEnergyKWh(float e_tod_energy_kwh){
		tod_energy_kwh.nValue = e_tod_energy_kwh;
		tod_energy_kwh.updateFlag = true;
	}
	void updateTodMaxDemandKVA(float e_tod_max_demand_kva){
		tod_max_demand_kva.nValue = e_tod_max_demand_kva;
		tod_max_demand_kva.updateFlag = true;
	}
	void updateCumulMaxDemand(float e_cumul_max_demand){
		cumul_max_demand.nValue = e_cumul_max_demand;
		cumul_max_demand.updateFlag = true;

	}
	void updateTamperSwitchStat(char e_tamper_switch_stat[]){
		strcpy(tamper_switch_icon.stateTxt.sValue,e_tamper_switch_stat);
		strcat(tamper_switch_icon.stateTxt.sValue,"\0");
		tamper_switch_icon.stateTxt.updateFlag = true;
	}
	void updateLastOccurOfAbn(char e_last_occur_of_abn[]){
		strcpy(last_occur_of_abn.sValue,e_last_occur_of_abn);
		strcat(last_occur_of_abn.sValue,"\0");
		last_occur_of_abn.updateFlag = true;
	}
	void updateLtstRestnOfAbn(char e_ltst_restn_of_abn[]){
		strcpy(ltst_restn_of_abn.sValue,e_ltst_restn_of_abn);
		strcat(ltst_restn_of_abn.sValue,"\0");
		ltst_restn_of_abn.updateFlag = true;
	}

	parameter getParameter(uint8_t param_num){
		parameter temp_param;
		switch (param_num) {
			case 0:
				temp_param = lcd_test_txt;
				lcd_test_txt.updateFlag = false;
				break;
			case 1:
				temp_param = date;
				date.updateFlag = false;
				break;
			case 2:
				temp_param = real_time;
				real_time.updateFlag = false;
				break;
			case 3:
				temp_param = active_energy;
				active_energy.updateFlag = false;
				break;
			case 4:
				temp_param = inst_kw;
				inst_kw.updateFlag = false;
				break;
			case 5:
				temp_param = reactive_energy;
				reactive_energy.updateFlag = false;
				break;
			case 6:
				temp_param = inst_kvar;
				inst_kvar.updateFlag = false;
				break;
			case 7:
				temp_param = apparent_energy;
				apparent_energy.updateFlag = false;
				break;
			case 8:
				temp_param = inst_kva;
				inst_kva.updateFlag = false;
				break;
			case 9:
				temp_param = max_demand_kw;
				max_demand_kw.updateFlag = false;
				break;
			case 10:
				temp_param = max_demand_kva;
				max_demand_kva.updateFlag = false;
				break;
			case 11:
				temp_param = md_reset_count;
				md_reset_count.updateFlag = false;
				break;
			case 12:
				temp_param = inst_pf;
				inst_pf.updateFlag = false;
				break;
			case 13:
				temp_param = inst_freq;
				inst_freq.updateFlag = false;
				break;
			case 14:
				temp_param = inst_voltage;
				inst_voltage.updateFlag = false;
				break;
			case 15:
				temp_param = inst_current;
				inst_current.updateFlag = false;
				break;
			case 16:
				temp_param = billing;
				billing.updateFlag = false;
				break;
			case 17:
				temp_param = tod_energy_kwh;
				tod_energy_kwh.updateFlag = false;
				break;
			case 18:
				temp_param = tod_max_demand_kva;
				tod_max_demand_kva.updateFlag = false;
				break;
			case 19:
				temp_param = cumul_max_demand;
				cumul_max_demand.updateFlag = false;
				break;
			case 20:
				temp_param = tamper_switch_icon.stateTxt;
				tamper_switch_icon.stateTxt.updateFlag = false;
				break;
			case 21:
				temp_param = last_occur_of_abn;
				last_occur_of_abn.updateFlag = false;
				break;
			case 22:
				temp_param = ltst_restn_of_abn;
				ltst_restn_of_abn.updateFlag = false;
				break;
			default:
				temp_param = error_parameter;
				error_parameter.updateFlag = false;
				break;
		}
		return temp_param;
	}
#endif





