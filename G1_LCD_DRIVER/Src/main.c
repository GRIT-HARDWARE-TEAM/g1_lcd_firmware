
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f7xx_hal.h"
#include "cmsis_os.h"
#include "tim.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "lcd_driver.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void MX_FREERTOS_Init(void);


static void prvSetupHardware( void );
//static void vLEDTimerCallback(TimerHandle_t);
//static void vTenantTimerCallback(TimerHandle_t);
//static void vOverwriterTimerCallback(TimerHandle_t);

//static void prvblueLedBlinkerTask( void *pvParameters );
//static void prvredLedBlinkerTask( void *pvParameters );

//static void prvIdleTask(void *pvParameters );
static void prvmenuNavigatorTask( void *pvParameters );

static void prvstestPrinterTask( void *pvParameters );
static void prvstestPrinter1Task( void *pvParameters );
static void prvstestPrinter2Task( void *pvParameters );
//static void prvstestOverwriterTask( void *pvParameters );
//static void prvitemTask( void *pvParameters );
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

//static TimerHandle_t xLEDTimer = NULL;
//static TimerHandle_t xtenantTimer = NULL;
//static TimerHandle_t xOverwriterTimer = NULL;
//static QueueHandle_t xLEDserviceMessageQueue = NULL;

const TickType_t timeToWaitTicks = portMAX_DELAY;
// const TickType_t timeToWaitTicks = pdMS_TO_TICKS( 100 );
//const TickType_t tenantTimeOut = pdMS_TO_TICKS( 1000 );

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  //HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  //SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  //MX_GPIO_Init();
  //MX_TIM7_Init();
  /* USER CODE BEGIN 2 */
  //setup();
  prvSetupHardware();
  xTaskCreate( prvmenuNavigatorTask, "menuNavigatorTask", (uint16_t)512, NULL, 1, NULL );
  xTaskCreate( prvstestPrinter2Task, "testPrinter2", (uint16_t)256, NULL, 5, NULL );
  xTaskCreate( prvstestPrinter1Task, "testPrinter1", (uint16_t)256, NULL, 4, NULL );
  xTaskCreate( prvstestPrinterTask, "testPrinter", (uint16_t)256, NULL, 3, NULL );

  //xLEDTimer = xTimerCreate( "LEDTimer", 									/* A text name, purely to help debugging. */
  //					  ( BACKLIGHT_TIMEOUT / portTICK_PERIOD_MS ),		/* The timer period, in this case 10000ms (10s). */
  //						  pdFALSE,										/* This is a one shot timer, so xAutoReload is set to pdFALSE. */
  //						  ( void * ) 0,									/* The ID is not used, so can be set to anything. */
  //						  vLEDTimerCallback								/* The callback function that switches the LED off. */
  //						);
  /*
  	xtenantTimer = xTimerCreate( "tenantTimer",
  								 tenantTimeOut,
  								 pdTRUE,
  								 ( void * ) 0,
  								 vTenantTimerCallback
  								);*/
  	//xTimerStart(xLEDTimer,0);
  	//xTimerStart(xtenantTimer,0);
  	vTaskStartScheduler();
  /* USER CODE END 2 */

  /* Call init function for freertos objects (in freertos.c) */
  //MX_FREERTOS_Init();

  /* Start scheduler */
  //osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
	  //HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);
	  //print("GRIT SYSTEMS G1 Mk1\n",1,2,BLACK,1,WHITE);
	  //TM_HD44780_Puts(1,2,"GRIT SYSTEMS G1 Mk1\n");
	  //HAL_Delay(500);
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 200;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Activate the Over-Drive mode 
    */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* USER CODE BEGIN 4 */

static void prvSetupHardware( void )
{
	//SCB_EnableICache();
	//SCB_EnableDCache();
	HAL_Init();
	SystemClock_Config();
	MX_GPIO_Init();
	//MX_TIM4_Init();
	MX_TIM7_Init();
	setup();
}

/*
static void vLEDTimerCallback( TimerHandle_t xTimer )
{
	//The timer has expired - so no button pushes have occurred in the last
	//ten seconds - turn the backlight LED off.
	//backlight(false);
	backlight(true);
}
*/

/*
static void vTenantTimerCallback(TimerHandle_t xTimer)
{
	incrementTenant();
}*/


/*
static void vOverwriterTimerCallback(TimerHandle_t xTimer){
	clear();
}*/

/*
 *	toggles the BLUE LED once every second.
 *
*/
/*
static void prvblueLedBlinkerTask( void *pvParameters )
{


	for( ;; )
	{

		HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);
		vTaskDelay(1000/portTICK_PERIOD_MS);

	}
}
*/


/*
 *	toggles the RED LED twice every second.
*/
/*
static void prvredLedBlinkerTask( void *pvParameters )
{


	for( ;; )
	{

		HAL_GPIO_TogglePin(LD3_GPIO_Port,LD3_Pin);
		vTaskDelay(500/portTICK_PERIOD_MS);
	}
}*/

/* prvIdleTask
 * */
/*
static void prvIdleTask(void *pvParameters ){
	for( ;; )
	{
		HAL_GPIO_TogglePin(LD3_GPIO_Port,LD3_Pin);
		vTaskDelay(1000/portTICK_PERIOD_MS);
	}
}*/

/*
 *	menuNavigatorTask cycles through
 *	the menu pages in response to a button press.
*/

static void prvmenuNavigatorTask( void *pvParameters )
{
	//BaseType_t xQueueReceiveStatus = (BaseType_t) NULL;
	// read service messages from the Queue.
	intmax_t parameter_number = -1;
	//serviceMessage xservice_messages[3];
	//uint16_t serviceMessageDelay = 0;
	int counter = 0;
	bool navFlag = false;

	for( ;; )
	{

		if (HAL_GPIO_ReadPin(USER_Btn_GPIO_Port,USER_Btn_Pin) == GPIO_PIN_SET){
			// software debounce
			vTaskDelay(100/portTICK_PERIOD_MS);	// wait for oscillations to pass
			// check again if the button is still in the same state
			if (HAL_GPIO_ReadPin(USER_Btn_GPIO_Port,USER_Btn_Pin) == GPIO_PIN_SET){

				/*
				 * dummy counter loop. may use an actual timer later...
				 * every count may represent a ms.
				 *
				 * */
				while(HAL_GPIO_ReadPin(USER_Btn_GPIO_Port,USER_Btn_Pin) == GPIO_PIN_SET){
					counter++;
					vTaskDelay(1/portTICK_PERIOD_MS);
				}
				/*
				if(counter <= 550){
					parameter_number = parameter_number + 1;	// go forward in the menu
				}
				else{
					parameter_number = parameter_number - 1;	// go backward in the menu
				}*/
				parameter_number++;	// for testing purposes only!!!

				counter = 0;
				backlight(true);
				//xTimerReset(xLEDTimer,10);
				navFlag = true;
				//xTimerReset(xOverwriterTimer,10);
			}
		}

		else{
			navFlag = false;
		}

		if(navFlag){
			// shows all values for 4 tenants every 15s for 2 params. on a page using a timer

			// Has to be MAX_NO_OF_TENANTS - 1 because of 0-Indexing.
			/*
			if(getTenant() > MAX_NO_OF_TENANTS - 1){
				resetTenant();
			}*/
			printMenuPage(&parameter_number);

		}

		else{

			// shows all values for 4 tenants every 15s for 2 params. on a page using a timer

			// Has to be MAX_NO_OF_TENANTS - 1 because of 0-Indexing.
			/*
			if(getTenant() > MAX_NO_OF_TENANTS - 1){
				resetTenant();
			}*/
			printMenuPage(&parameter_number);

		}

		vTaskDelay(1/portTICK_PERIOD_MS);
	}
}

/*
static void prvtitleTask( void *pvParameters )
{

	//uint8_t counter = 0;
	for( ;; )
	{
		clear();
		printMenuPageTitle(21);
		vTaskDelay(1/portTICK_PERIOD_MS);
	}
}
*/
/*
static void prvitemTask( void *pvParameters )
{

	//uint8_t counter = 0;
	for( ;; )
	{
		clear();
		printMenuPageItem(21);
		vTaskDelay(1/portTICK_PERIOD_MS);
	}
}*/



/*
 *	prints the value of the counter is on y = 70.
*/


static void prvstestPrinter2Task( void *pvParameters )
{
	//int8_t counter = 0;
	//BaseType_t xqueueSendStatus2 = (BaseType_t) NULL;
	//bool displayServiceMessage = true;
	for(;;)
	{
		/*
		char txt[30] = {"The value of counter is: "};
		counter++;
		if(counter > 99){
			counter = 0;
		}
		sprintf(txt,"Counter is: %d",counter);
		serviceMessage serv_msgTx2;
		strcpy(serv_msgTx2.message,txt);
		serv_msgTx2.x = 40;
		serv_msgTx2.y = 60;
		serv_msgTx2.delay = 300;
		if(displayServiceMessage){
			xQueueSend(xLEDserviceMessageQueue,&serv_msgTx2, timeToWaitTicks );
			displayServiceMessage = false;
		}*/
		updateCSIconState(NOT_OK);
		updateTSIconState(MAGNETIC_TAMPER_EVENT);
		updateLCDTestTxt("TEST");
		updateDate("270219");
		updateRealTime("16:15");
		updateActiveEnergy(5.74);
		updateReactiveEnergy(3.924);
		updateApparentEnergy(3924);
		updateMaxDemandKW(17.8);
		updateMaxDemandKVA(9745.4);
		updateMDResetCount(5);
		updateInstPF(0.99);
		updateInstFreq(49.999);
		updateInstVoltage(220);
		updateInstCurrent(2);
		updateInstKW(2.0);
		updateInstKVAr(5.87);
		updateInstKVA(87.9);
		updateBilling("136.00");
		updateTodEnergyKWh(78.4);
		updateTodMaxDemandKVA(2563.4);
		updateCumulMaxDemand(73.9);
		//updateTamperSwitchStat("ERROR!");
		updateLastOccurOfAbn("020494");
		updateLtstRestnOfAbn("270219");
		vTaskDelay(1000/portTICK_PERIOD_MS);
	}

}

/*
 *	prints the value of the counter is on y = 160.
*/



static void prvstestPrinter1Task( void *pvParameters )
{

	//int8_t counter = 0;
	//BaseType_t xqueueSendStatus2 = (BaseType_t) NULL;
	//bool displayServiceMessage = true;
	for(;;)
	{
		/*
		char txt[30] = {"The value of counter 1 is: "};
		counter++;
		if(counter > 99){
			counter = 0;
		}

		sprintf(txt,"Counter 1 is: %d",counter);
		serviceMessage serv_msgTx2;
		strcpy(serv_msgTx2.message,txt);
		serv_msgTx2.x = 40;
		serv_msgTx2.y = 80;
		serv_msgTx2.delay = 100;
		if(displayServiceMessage){
			xQueueSend(xLEDserviceMessageQueue,&serv_msgTx2, timeToWaitTicks );
			displayServiceMessage = false;
		}*/
		updateCSIconState(OK);
		updateOCIconState(OK);
		updateTSIconState(OK);
		updateOVIconState(OK);
		updateInstFreq(50.999);
		updateLCDTestTxt("TEST1");
		updateDate("280219");
		updateRealTime("17:15");
		updateActiveEnergy(6.74);
		updateReactiveEnergy(4.924);
		updateApparentEnergy(4924);
		updateMaxDemandKW(27.8);
		updateMaxDemandKVA(10745.4);
		updateMDResetCount(6);
		updateInstPF(1.99);
		updateInstVoltage(221);
		updateInstCurrent(3);
		updateInstKW(2.1);
		updateInstKVAr(6.87);
		updateInstKVA(97.9);
		updateBilling("137.00");
		updateTodEnergyKWh(88.4);
		updateTodMaxDemandKVA(3563.4);
		updateCumulMaxDemand(83.9);
		//updateTamperSwitchStat("ERROR!");
		updateLastOccurOfAbn("120494");
		updateLtstRestnOfAbn("370219");
		vTaskDelay(1700/portTICK_PERIOD_MS);
	}
}

/*
 *	prints the value of the counter is on y = 190.
*/

static void prvstestPrinterTask( void *pvParameters )
{

	//int8_t counter = 0;
	//BaseType_t xqueueSendStatus2 = (BaseType_t) NULL;
	//bool displayServiceMessage = true;
	for(;;)
	{
		/*
		char txt[30] = {"The value of counter 2 is: "};
		counter++;
		if(counter > 99){
			counter = 0;
		}
		sprintf(txt,"Counter 2 is: %d",counter);
		serviceMessage serv_msgTx2;
		strcpy(serv_msgTx2.message,txt);
		serv_msgTx2.x = 40;
		serv_msgTx2.y = 100;
		serv_msgTx2.delay = 50;
		if(displayServiceMessage){
			xQueueSend(xLEDserviceMessageQueue,&serv_msgTx2, timeToWaitTicks );
			displayServiceMessage = false;
		}*/

		updateOVIconState(OVER_VOLTAGE_FAULT);
		updateTSIconState(CASE_TAMPER_EVENT);
		updateOCIconState(OVER_CURRENT_FAULT);
		//updateCSIconState(NOT_OK);
		updateInstFreq(50);
		updateLCDTestTxt("TEST2");
		updateDate("260219");
		updateRealTime("15:15");
		updateActiveEnergy(4.74);
		updateReactiveEnergy(2.924);
		updateApparentEnergy(1924);
		updateMaxDemandKW(7.8);
		updateMaxDemandKVA(8745.4);
		updateMDResetCount(4);
		updateInstPF(0.89);
		updateInstFreq(49.999);
		updateInstVoltage(219);
		updateInstCurrent(1);
		updateInstKW(1.0);
		updateInstKVAr(4.87);
		updateInstKVA(77.9);
		updateBilling("126.00");
		updateTodEnergyKWh(68.4);
		updateTodMaxDemandKVA(1563.4);
		updateCumulMaxDemand(63.9);
		//updateTamperSwitchStat("ERROR!");
		updateLastOccurOfAbn("010494");
		updateLtstRestnOfAbn("250219");
		vTaskDelay(2100/portTICK_PERIOD_MS);
	}
}

/*
static void prvstestOverwriterTask( void *pvParameters )
{
	for( ;; )
	{
		char txt[] = {"Overwritten "};
		xSemaphoreTake( xMutex, ( TickType_t ) 10  );
		{
			// COLOUR CODES PASSED IN MANUALLY
			print(txt,5,70,0xFFFF ,3,0x0000 );
		}
		xSemaphoreGive( xMutex );
		vTaskDelay(1/portTICK_PERIOD_MS);
	}
}
*/

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
